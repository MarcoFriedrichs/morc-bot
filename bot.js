const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const GoogleImages = require('google-images');
const icon = "https://cdn.discordapp.com/avatars/396772000422690817/ccd15a1c3f40a31c1eb573ad371756ed.jpg?size=1024";
const googleClient = new GoogleImages(process.env.GOOGLE_CSE_ID, process.env.GOOGLE_CSE_API_KEY);
const path = require('path');
const axios = require("axios");


let url;
let lang = "de";
let tts;
let imageURLs;
let prefix = config.prefix;
let messageText;
let selectedUser;
let adminUsers;
let permissionDenied = true; //reset
var googleTTS = require('google-tts-api');
let streamUrl;
let connection;




function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}





client.on("ready", () => {
  console.log("I am ready!");
});

client.on("message", (message) => {
    messageText = message.content;

    let adminRole = message.guild.roles.find("name", "morcBotAdmin");





    checkForPermission = function () {
        if(!message.member.roles.has(adminRole.id)) {
            message.channel.send("`You're not premitted to do this! Ask an Admin for help.`");
            return false;
            permissionDenied = true;
            console.log(permissionDenied);
        }
        else {
            return true;
            permissionDenied = false
        }
    }

    if (!messageText.startsWith(prefix)) return;


    //change Prefix of the Bot
    if (messageText.startsWith(prefix + config.command[0]) && checkForPermission() == true) {

        prefix = messageText.slice(prefix.length + config.command[0].length + 1);
        config.prefix = prefix;
        fs.writeFile("./config.json", JSON.stringify(config), (err) => console.error);
        message.channel.send("`Changed the prefix to: '" + prefix + "'`");
    }
    else if (messageText.startsWith(prefix + config.command[1] + " ")) {
        if(messageText.length > (prefix.length + config.command[1].length + 1)){



            let rand = getRandomInt(0, 10);
            console.log(rand)
            googleClient.search(messageText.slice(prefix.length + config.command[1].length + 1))
                .then(images => {
                    message.channel.send({embed: {
                        "author": {
                            "name": "show original website",
                            "url": images[rand].parentPage,
                            "icon_url": icon
                        },
                        "color": 14935011,
                        "image": {
                            "url": images[rand].url
                        }
                    }})
            });
        }
        else {
            message.channel.send("`Syntax Error, use " + prefix + config.command[1] + " <Search>`")
        }
    }
    else if (messageText.startsWith(prefix + config.command[2] + " ")) {
        if(messageText.length > (prefix.length + config.command[2].length + 1)){
            googleClient.search(messageText.slice(prefix.length + config.command[2].length + 1))
            .then(images => {
                message.channel.send({embed: {
                    "author": {
                        "name": "show original website",
                        "url": images[0].parentPage,
                        "icon_url": icon
                    },
                    "color": 14935011,
                    "image": {
                        "url": images[0].url
                    },
                }})
            });
        }
        else {
            message.channel.send("`Syntax Error, use " + prefix + config.command[2] + " <Search>`")
        }
    }


    else if (messageText.startsWith(prefix + config.command[3]) && messageText.length > prefix.length + config.command[3].length + 1) {

        tts = messageText.slice(prefix.length + config.command[3].length + 4);
        lang = messageText.slice(prefix.length + config.command[3].length + 1, (tts.length + 1) * -1);

        connection = message.member.voiceChannel.connection;


        console.log(tts.length);
        console.log(Math.ceil(tts.length / 200));

        if(message.member.voiceChannel.connection !== null) {
            message.member.voiceChannel.connection.playArbitraryInput(streamUrl).end();
        }

                //console.log(dispatcher)
                //dispatcher.end();
        var ttsSplit = tts.match(/.{1,200}(?=(.{200})+(?!.))|.{1,200}$/g);

        console.log(ttsSplit);

        googleTTS(tts.slice(0, 200), lang, 1)
            .then(function (url) {
                streamUrl = url + "/tts.mp3";
            })
            .catch(function (err) {
              console.error(err.stack);
            });

        message.member.voiceChannel.join()
            .then((connection, url) => {





                dispatcher = connection.playArbitraryInput(streamUrl).on('end', () => {
                    if (ttsSplit.length >= 2){

                    }
                    else {
                        message.member.voiceChannel.leave();
                        console.log("play");
                    }
                });
            });



    }

    else if (messageText.startsWith(prefix + config.command[4] + " ")) {
        if(messageText.length > (prefix.length + config.command[4].length + 1)){

          axios.get('https://api.sketchfab.com/v3/search?type=models&q=' + messageText.slice(prefix.length + config.command[4].length + 1))
            .then(function(response) {
              var viewer = response.data.results[getRandomInt(0, 10)].viewerUrl;
              message.channel.send(viewer + '/embed?autostart=1');



            })
            .catch(error => {
              console.log(error);
            });
        }
        else {
            message.channel.send("`Syntax Error, use " + prefix + config.command[4] + " <Search>`")
        }
    }

    //if invalid command
    else if (messageText.startsWith(prefix) && permissionDenied == false) {
        message.channel.send("I didn't understand you");
    }


});

client.login(process.env.BOT_TOKEN);
